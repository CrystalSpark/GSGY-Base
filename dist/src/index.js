"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var MySQLClient_1 = __importDefault(require("./GSGY/Database/MySQL/MySQLClient"));
var MySQLConnection_1 = __importDefault(require("./GSGY/Database/MySQL/MySQLConnection"));
var SQLConnection_1 = __importDefault(require("./GSGY/Database/SQLConnection"));
var SQLModel_1 = __importDefault(require("./GSGY/Database/SQLModel"));
var SQLClient_1 = __importDefault(require("./GSGY/Database/SQLClient"));
var Message_1 = __importDefault(require("./GSGY/Model/Message"));
var Config_1 = __importDefault(require("./GSGY/Config"));
var Log_1 = __importDefault(require("./GSGY/Log"));
var Util_1 = __importDefault(require("./GSGY/Util"));
var GSGY = /** @class */ (function () {
    function GSGY() {
    }
    GSGY.Database = {
        SQLConnection: SQLConnection_1.default,
        SQLClient: SQLClient_1.default,
        SQLModel: SQLModel_1.default,
        MySQL: {
            MySQLConnection: MySQLConnection_1.default,
            MySQLClient: MySQLClient_1.default
        }
    };
    GSGY.Model = {
        Message: Message_1.default
    };
    GSGY.Config = Config_1.default;
    GSGY.Log = Log_1.default;
    GSGY.Util = Util_1.default;
    return GSGY;
}());
exports.default = GSGY;
//# sourceMappingURL=index.js.map