/**
 * 配置模块
 */
export default class Config {
    /**
     * 读取配置
     * @param path 配置名称
     */
    static get(path: string): any;
}
//# sourceMappingURL=Config.d.ts.map