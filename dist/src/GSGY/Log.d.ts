import 'datejs';
/**
 * 日志模块
 */
export default class Log {
    /**
     * 日志路径
     * @desc 从配置文件取，如果配置文件中没有，则默认在项目根目录/log下
     */
    static logPath: string;
    /**
     * 写日志（异常）
     * @param {Error} err 异常对象
     * @param {String} errMsg 异常信息
     */
    static write(err: Error, errMsg: string): void;
    /**
     * 写日志（普通）
     * @param {Object|String} obj 可序列化的对象或者文本
     */
    static write(obj: Object): void;
}
//# sourceMappingURL=Log.d.ts.map