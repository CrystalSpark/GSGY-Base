"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = __importDefault(require("lodash"));
var Message_1 = __importDefault(require("../../Model/Message"));
var MySQLConnection_1 = __importDefault(require("./MySQLConnection"));
var Config_1 = __importDefault(require("../../Config"));
var SQLClient_1 = __importDefault(require("../SQLClient"));
var Log_1 = __importDefault(require("../../Log"));
/**
 * @class MySQLClient 数据库操作类
 * @property {MySQLConnection} connection 数据库连接对象
 */
var MySQLClient = /** @class */ (function (_super) {
    __extends(MySQLClient, _super);
    function MySQLClient(connection) {
        var _this = this;
        if (connection instanceof MySQLConnection_1.default) {
            _this = _super.call(this, connection) || this;
            _this.connection = connection;
        }
        else if (typeof connection == 'string') {
            _this = _super.call(this, connection) || this;
            _this.connection = new MySQLConnection_1.default(Config_1.default.get(connection));
        }
        else {
            throw Error('初始化数据库连接参数不正确');
        }
        return _this;
    }
    MySQLClient.prototype._insertSingle = function (model) {
        return __awaiter(this, void 0, void 0, function () {
            var result, sql, keyArr, valArr, key, sqlRes, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = new Message_1.default();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        sql = "INSERT INTO " + model.getTable() + " ";
                        keyArr = [], valArr = [];
                        for (key in model) {
                            if (model.hasOwnProperty(key)) {
                                keyArr.push(key);
                                valArr.push(model[key]);
                            }
                        }
                        sql += "(" + keyArr.join(',') + ") VALUES (" + valArr.map(function () { return '?'; }).join(',') + ")";
                        Log_1.default.write('sql _insertSingle/sql: ' + sql);
                        return [4 /*yield*/, this.connection.query(sql, valArr)];
                    case 2:
                        sqlRes = (_a.sent())[0];
                        if (sqlRes.affectedRows > 0) {
                            result.content = sqlRes;
                            result.message = '新增数据成功！';
                            result.status = Message_1.default.STATUS.SUCCESS;
                        }
                        else {
                            Log_1.default.write('sql insert/empty: ' + sql);
                            result.content = sqlRes;
                            result.errCode = SQLClient_1.default.CONST.ERROR_CODE.INSERT_NOTHING;
                            result.message = '没有新增任何数据！';
                            result.status = Message_1.default.STATUS.FAIL;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        Log_1.default.write(e_1, 'sql insert/error');
                        result.content = e_1;
                        result.message = '新增数据失败！';
                        result.status = Message_1.default.STATUS.FAIL;
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MySQLClient.prototype.insert = function (target) {
        return __awaiter(this, void 0, void 0, function () {
            var result, that, _i, target_1, el, _a, e_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        result = new Message_1.default();
                        that = this;
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 10, , 11]);
                        if (!lodash_1.default.isArray(target)) return [3 /*break*/, 6];
                        _i = 0, target_1 = target;
                        _b.label = 2;
                    case 2:
                        if (!(_i < target_1.length)) return [3 /*break*/, 5];
                        el = target_1[_i];
                        return [4 /*yield*/, that._insertSingle(el)];
                    case 3:
                        _b.sent();
                        _b.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [3 /*break*/, 8];
                    case 6: return [4 /*yield*/, that._insertSingle(target)];
                    case 7:
                        _b.sent();
                        _b.label = 8;
                    case 8:
                        // 执行sql
                        _a = result;
                        return [4 /*yield*/, this.connection.commit()];
                    case 9:
                        // 执行sql
                        _a.content = _b.sent();
                        result.message = '新增数据成功！';
                        result.status = Message_1.default.STATUS.SUCCESS;
                        return [3 /*break*/, 11];
                    case 10:
                        e_2 = _b.sent();
                        Log_1.default.write(e_2, 'sql insert/error');
                        result.content = e_2;
                        result.message = '新增数据失败！';
                        result.status = Message_1.default.STATUS.FAIL;
                        return [3 /*break*/, 11];
                    case 11: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 删除记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    MySQLClient.prototype.delete = function (model) {
        return __awaiter(this, void 0, void 0, function () {
            var result, table, sql, whereArr, valArr, key, sqlRes, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = new Message_1.default();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        table = model.getTable();
                        sql = "DELETE FROM " + table + " ";
                        whereArr = [], valArr = [];
                        for (key in model) {
                            if (model.hasOwnProperty(key) && !lodash_1.default.isNil(model[key])) {
                                whereArr.push(key + " = ?");
                                valArr.push(model[key]);
                            }
                        }
                        // 数组非空
                        if (!lodash_1.default.isEmpty(whereArr)) {
                            sql += "WHERE " + whereArr.join(' and ');
                        }
                        Log_1.default.write('sql delete/sql: ' + sql);
                        return [4 /*yield*/, this.connection.query(sql, valArr)];
                    case 2:
                        sqlRes = (_a.sent())[0];
                        if (sqlRes.affectedRows > 0) {
                            result.content = sqlRes;
                            result.message = '删除数据成功！';
                            result.status = Message_1.default.STATUS.SUCCESS;
                        }
                        else {
                            Log_1.default.write('sql delete/empty: ' + sql);
                            result.content = sqlRes;
                            result.errCode = SQLClient_1.default.CONST.ERROR_CODE.DELETE_NOT_FOUND;
                            result.message = '没有删除任何数据！';
                            result.status = Message_1.default.STATUS.FAIL;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        Log_1.default.write(e_3, 'sql delete/error');
                        result.content = e_3;
                        result.message = '删除数据失败！';
                        result.status = Message_1.default.STATUS.FAIL;
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 查询单条记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    MySQLClient.prototype.select = function (model) {
        return __awaiter(this, void 0, void 0, function () {
            var result, table, sql, whereArr, valArr, key, sqlRes, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = new Message_1.default();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        table = model.getTable();
                        sql = "SELECT * FROM " + table + " ";
                        whereArr = [], valArr = [];
                        for (key in model) {
                            if (model.hasOwnProperty(key) && !lodash_1.default.isNil(model[key])) {
                                whereArr.push(key + " = ?");
                                valArr.push(model[key]);
                            }
                        }
                        // 数组非空
                        if (!lodash_1.default.isEmpty(whereArr)) {
                            sql += "WHERE " + whereArr.join(' and ');
                        }
                        Log_1.default.write('sql select/sql: ' + sql);
                        return [4 /*yield*/, this.connection.query(sql, valArr)];
                    case 2:
                        sqlRes = (_a.sent())[0];
                        if (!lodash_1.default.isEmpty(sqlRes)) {
                            result.content = model.constructor.deserializeList(sqlRes);
                            result.message = '查询数据成功！';
                            result.status = Message_1.default.STATUS.SUCCESS;
                        }
                        else {
                            Log_1.default.write('sql select/empty: ' + sql);
                            result.content = sqlRes;
                            result.errCode = SQLClient_1.default.CONST.ERROR_CODE.SELECT_NOT_FOUND;
                            result.message = '没有查询到任何数据！';
                            result.status = Message_1.default.STATUS.FAIL;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_4 = _a.sent();
                        Log_1.default.write(e_4, 'sql select/error');
                        result.content = e_4;
                        result.message = '查询数据失败！';
                        result.status = Message_1.default.STATUS.FAIL;
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 查询记录分页
     * @param {SQLModel} model
     * @param pageIndex
     * @param pageSize
     * @return {Promise<Message>}
     */
    MySQLClient.prototype.page = function (model, pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 1; }
        if (pageSize === void 0) { pageSize = 8; }
        return __awaiter(this, void 0, void 0, function () {
            var result, table, sql, sqlTotal, whereArr, valArr, key, sqlResTotal, sqlRes, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = new Message_1.default();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        table = model.getTable();
                        sql = "SELECT * FROM " + table + " ";
                        sqlTotal = "SELECT count(1) as total FROM " + table + " ";
                        whereArr = [], valArr = [];
                        for (key in model) {
                            if (model.hasOwnProperty(key) && !lodash_1.default.isNil(model[key])) {
                                whereArr.push(key + " = ?");
                                valArr.push(model[key]);
                            }
                        }
                        // 数组非空
                        if (!lodash_1.default.isEmpty(whereArr)) {
                            sql += "WHERE " + whereArr.join(' and ');
                            sqlTotal += "WHERE " + whereArr.join(' and ');
                        }
                        sql += " limit " + (pageIndex - 1) * pageSize + ", " + pageSize;
                        Log_1.default.write('sql page/total: ' + sqlTotal);
                        return [4 /*yield*/, this.connection.query(sqlTotal, valArr)];
                    case 2:
                        sqlResTotal = (_a.sent())[0];
                        Log_1.default.write('sql page/sql: ' + sql);
                        return [4 /*yield*/, this.connection.query(sql, valArr)];
                    case 3:
                        sqlRes = (_a.sent())[0];
                        result.content = {
                            total: sqlResTotal[0].total,
                            data: sqlRes
                        };
                        result.message = '分页查询数据成功！';
                        result.status = Message_1.default.STATUS.SUCCESS;
                        return [3 /*break*/, 5];
                    case 4:
                        e_5 = _a.sent();
                        Log_1.default.write(e_5, 'sql page/error');
                        result.content = e_5;
                        result.message = '分页查询数据失败！';
                        result.status = Message_1.default.STATUS.FAIL;
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 根据主键修改记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    MySQLClient.prototype.update = function (model) {
        return __awaiter(this, void 0, void 0, function () {
            var result, primaryKey, table, sql, keyArr, valArr, key, _a, e_6;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        result = new Message_1.default();
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 5, , 6]);
                        primaryKey = model.getPrimaryKey();
                        table = model.getTable();
                        sql = "UPDATE " + table + " ";
                        keyArr = [], valArr = [];
                        for (key in model) {
                            if (model.hasOwnProperty(key) && key != primaryKey) {
                                keyArr.push(key);
                                valArr.push(model[key]);
                            }
                        }
                        if (!lodash_1.default.isEmpty(keyArr)) {
                            sql += " SET " + keyArr.map(function (el) { return el + " = ?"; }).join(' , ') + " ";
                        }
                        sql += " WHERE " + primaryKey + " = ?";
                        if (!(primaryKey in model)) return [3 /*break*/, 3];
                        valArr.push(model[model.getPrimaryKey()]);
                        Log_1.default.write('sql update/sql: ' + sql);
                        // 执行sql
                        _a = result;
                        return [4 /*yield*/, this.connection.query(sql, valArr)];
                    case 2:
                        // 执行sql
                        _a.content = _b.sent();
                        result.message = '更新数据成功！';
                        result.status = Message_1.default.STATUS.SUCCESS;
                        return [3 /*break*/, 4];
                    case 3:
                        Log_1.default.write('sql update/primaryKey undefined');
                        result.content = {};
                        result.message = '更新数据失败，主键未定义！';
                        result.status = SQLClient_1.default.CONST.ERROR_CODE.PRIMARY_KEY_UNDEFINED;
                        _b.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        e_6 = _b.sent();
                        Log_1.default.write(e_6, 'sql update/error');
                        result.content = e_6;
                        result.message = '更新数据失败！';
                        result.status = Message_1.default.STATUS.FAIL;
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 运行SQL语句
     * @param {string} sql
     * @param {Array<string>} [params]
     * @return {Promise<Message>}
     */
    MySQLClient.prototype.runSQL = function (sql, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, sqlRes, e_7;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = new Message_1.default();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        params && params.forEach(function (param, index) {
                            if (!lodash_1.default.isNil(param)) {
                                sql = sql.replace('${' + index + '}', _this.connection.escape(param));
                            }
                        });
                        Log_1.default.write('sql runSQL/sql: ' + sql);
                        return [4 /*yield*/, this.connection.query(sql)];
                    case 2:
                        sqlRes = (_a.sent())[0];
                        result.content = sqlRes;
                        result.message = '运行SQL语句成功！';
                        result.status = Message_1.default.STATUS.SUCCESS;
                        return [3 /*break*/, 4];
                    case 3:
                        e_7 = _a.sent();
                        Log_1.default.write(e_7, 'sql runSQL/error');
                        result.content = e_7;
                        result.message = '运行SQL语句失败！';
                        result.status = Message_1.default.STATUS.FAIL;
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 连接数据库
     */
    MySQLClient.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.connect()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 断开数据库连接
     */
    MySQLClient.prototype.disconnect = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.close()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 开始事务
     */
    MySQLClient.prototype.beginTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.beginTransaction()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 回滚事务
     */
    MySQLClient.prototype.rollback = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.rollback()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 提交事务
     */
    MySQLClient.prototype.commit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.commit()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return MySQLClient;
}(SQLClient_1.default));
exports.default = MySQLClient;
//# sourceMappingURL=MySQLClient.js.map