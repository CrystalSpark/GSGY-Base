import Message from '../Model/Message';
import SQLModel from './SQLModel';
import SQLConnection from './SQLConnection';
/**
 * @class MySQLClient 数据库操作类
 * @property {MySQLConnection} connection 数据库连接对象
 */
export default abstract class SQLClient {
    static CONST: {
        ERROR_CODE: {
            DELETE_NOT_FOUND: string;
            SELECT_NOT_FOUND: string;
            INSERT_NOTHING: string;
            PRIMARY_KEY_UNDEFINED: string;
            UNKNOWN: string;
        };
    };
    connection: SQLConnection | null;
    /**
     * @constructor 构造函数
     * @param {MySQLConnection} connection 数据库连接对象
     */
    protected constructor(connection: SQLConnection);
    /**
     * @constructor 构造函数
     * @param {string} connectionKey 配置名称
     */
    protected constructor(connectionKey: string);
    /**
     * 新增记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract insert<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 删除记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract delete<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 查询单条记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract select<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 查询记录分页
     * @param {SQLModel} model
     * @param pageIndex
     * @param pageSize
     * @return {Promise<Message>}
     */
    abstract page<T extends SQLModel>(model: T, pageIndex: number, pageSize: number): Promise<Message>;
    /**
     * 根据主键修改记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract update<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 运行SQL语句
     * @param {string} sql
     * @param {Array<string>} [params]
     * @return {Promise<Message>}
     */
    abstract runSQL(sql: string, params?: Array<string>): Promise<Message>;
}
//# sourceMappingURL=SQLClient.d.ts.map