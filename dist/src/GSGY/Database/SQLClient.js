"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Message_1 = __importDefault(require("../Model/Message"));
/**
 * @class MySQLClient 数据库操作类
 * @property {MySQLConnection} connection 数据库连接对象
 */
var SQLClient = /** @class */ (function () {
    function SQLClient(connection) {
        this.connection = null;
    }
    SQLClient.CONST = {
        ERROR_CODE: {
            DELETE_NOT_FOUND: 'delete_not_found',
            SELECT_NOT_FOUND: 'select_not_found',
            INSERT_NOTHING: 'insert_nothing',
            PRIMARY_KEY_UNDEFINED: 'primary_key_undefined',
            UNKNOWN: Message_1.default.ERROR_CODE.UNKNOWN
        }
    };
    return SQLClient;
}());
exports.default = SQLClient;
//# sourceMappingURL=SQLClient.js.map