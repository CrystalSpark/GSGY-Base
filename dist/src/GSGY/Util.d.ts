export default class Util {
    /**
     * 生成UUID（唯一ID）
     */
    static generateUUID(): string;
}
//# sourceMappingURL=Util.d.ts.map