export default class MiniProgram {
    APPID: string;
    APPSECRET: string;
    constructor(APPID: string, APPSECRET: string);
    getAccessToken(): void;
}
//# sourceMappingURL=MiniProgram.d.ts.map