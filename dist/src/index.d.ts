import MySQLClient from './GSGY/Database/MySQL/MySQLClient';
import MySQLConnection from './GSGY/Database/MySQL/MySQLConnection';
import SQLConnection from './GSGY/Database/SQLConnection';
import SQLModel from './GSGY/Database/SQLModel';
import SQLClient from './GSGY/Database/SQLClient';
import Message from './GSGY/Model/Message';
import Config from './GSGY/Config';
import Log from './GSGY/Log';
import Util from './GSGY/Util';
export default class GSGY {
    static Database: {
        SQLConnection: typeof SQLConnection;
        SQLClient: typeof SQLClient;
        SQLModel: typeof SQLModel;
        MySQL: {
            MySQLConnection: typeof MySQLConnection;
            MySQLClient: typeof MySQLClient;
        };
    };
    static Model: {
        Message: typeof Message;
    };
    static Config: typeof Config;
    static Log: typeof Log;
    static Util: typeof Util;
}
//# sourceMappingURL=index.d.ts.map