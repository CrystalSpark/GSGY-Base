# 更新日志

## v0.3.1
`2020-07-16`

- 增加了`MySQLClient`的事务相关操作，内容如下
 1. 增加了`beginTransaction`、`rollback`、`commit` 等事务操作API
- 将数据库`connect`、`close`操作抽取为单独的方法，现在在操作数据库前需要主动调用`connect`

- *此次修改与之前的版本无法兼容，因此更改了y版本号

---
## v0.2.2
`2020-07-05`

- 增加了`MySQLConnection`的事务相关操作，内容如下
 1. 增加了`beginTransaction`、`rollback`、`commit` 等事务操作API
- 增加了`insert`方法的数组重载，`MySQLClient`现在可以批量新增了
---
## v0.2.1
`2020-06-30`

- 删除了`GSGY.Model.Message`的静态属性`CONST`，原`GSGY.Model.Message.CONST`下面的常量现在都提升到了`GSGY.Model.Message`下

- *此次修改与之前的版本无法兼容，因此更改了y版本号

---
## v0.1.1
`2020-06-30`

- 重构了`GSGY.Database.SQLModel`的属性，内容如下：
1. 现在`table`、`primaryKey`成为静态属性了
2. 取消`column`字段，列名将成为实例的直接属性

- *此次修改与之前的版本无法兼容，因此更改了y版本号

---

## v0.0.28
`2020-06-27`

- 优化了`GSGY.Database.MySQL.MySQLClient`的相关返回提示(`res.message`)

- 修改了`GSGY.Database.MySQL.MySQLClient`的查询相关方法(`select`,`runSQL`)的返回内容，现在`res.content`返回查询结果的`SQLModel`列表

---
## v0.0.26
`2020-06-26`

- 添加了`SQLClient`直接运行SQL语句的方式`GSGY.Database.SQLClient.runSQL`：
- 修复了`GSGY.Util.generateUUID`的返回类型

---
## v0.0.22
`2020-06-26`

- 添加了工具类`GSGY.Util`，包括以下内容：
1. 完成了 `GSGY.Util.generateUUID` 生成UUID

- 修改了日志、配置项的相关函数名称，以统一命名规则
1. `GSGY.Config.fGetConfig` 现在改为了 `GSGY.Config.get`
2. `GSGY.Log.fWriteLog` 现在改为了 `GSGY.Config.write`

---



## v0.0.16 
`2020-06-07`

- 完成了数据库相关基础代码及MySQL相关实现，包括以下内容：
1. 完成了 `GSGY.DataBase.SQLClient` 数据库操作抽象类
2. 完成了 `GSGY.DataBase.MySQL.MySQLClient` 数据库操作抽象类的MySQL实现
3. 完成了 `GSGY.DataBase.SQLConnection` 数据库连接抽象类
4. 完成了 `GSGY.DataBase.MySQL.MySQLConnection` 数据库连接抽象类的MySQL实现
5. 完成了 `GSGY.DataBase.SQLModel` 数据库表抽象类

- 完成了日志、配置项、通用消息体的相关工具类，包括以下内容
1. 完成了 `GSGY.Config` 配置项读取工具类
2. 完成了 `GSGY.Log` 日志读写工具类
3. 完成了 `GSGY.Model.Message` 通用消息体工具类

---