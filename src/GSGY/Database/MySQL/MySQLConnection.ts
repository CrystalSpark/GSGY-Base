import SQLConnection from '../SQLConnection'
import mysql2 from 'mysql2'
import Log from '../../Log'

/**
 * @class MySQLConnection MySQL数据连接类
 * @implements SQLConnection
 */
export default class MySQLConnection extends SQLConnection {


    /**
     * @constructor 构造函数
     * @param {Object} config
     * @param {String} config.host 数据库地址
     * @param {Number} [config.port] 端口
     * @default 3306
     * @param {String} config.user 用户
     * @param {String} config.password 密码
     * @param {String} config.database 数据库名
     */
    constructor({host, port = 3306, user, password, database}
                    : { host: string, port: number, user: string, password: string, database: string }) {
        super({host, port, user, password, database})
    }

    /**
     * 连接数据库
     * @override
     */
    async connect(): Promise<any> {
        try {
            this.connection = await mysql2.createConnection({
                host: this.host,
                port: this.port,
                database: this.database,
                user: this.user,
                password: this.password
            }).promise()
        } catch (e) {
            Log.write(e, '数据库连接出错！')
        }
    }

    close() {
        return this.connection.close()
    }


    async query(sql: string, valArr?: Array<any>) {
        if (valArr && valArr.length) {
            return await this.connection.query(sql, valArr)
        }
        else{
            return await this.connection.query(sql)
        }
    }

    async beginTransaction(){
        return await this.connection.beginTransaction()
    }

    async rollback(){
        return await this.connection.rollback()
    }
    async commit(){
        return await this.connection.commit()
    }

    escape(target:string){
        return this.connection.escape(target)
    }


}