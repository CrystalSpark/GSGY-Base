import fs from 'fs'
import path from 'path'
import Config from './Config'
import _ from 'lodash'
import 'datejs'

/**
 * 日志模块
 */
export default class Log {
    /**
     * 日志路径
     * @desc 从配置文件取，如果配置文件中没有，则默认在项目根目录/log下
     */
    static logPath: string = Config.get('log') ||
        path.resolve(path.resolve(path.dirname(require.main?.filename||''), './logs'))

    /**
     * 写日志（异常）
     * @param {Error} err 异常对象
     * @param {String} errMsg 异常信息
     */
    static write(err: Error, errMsg: string): void;
    /**
     * 写日志（普通）
     * @param {Object|String} obj 可序列化的对象或者文本
     */
    static write(obj: Object): void;
    static write(obj: Object | Error | String, errMsg?: string): void {
        let log = `${new Date().toString('yyyy-MM-dd HH:mm:ss')}\n`
        if (_.isError(obj)) {
            console.error(obj)
            log += `发生错误！错误信息：${errMsg}\n`
            log += `错误栈：${obj.stack}`
        }
        else if (typeof obj == 'string') {
            log += `${obj}`
        } else {
            log += `${JSON.stringify(obj)}`
        }
        console.log(log)
        fs.mkdirSync(Log.logPath,{recursive:true} )
        fs.writeFileSync(path.resolve(Log.logPath , new Date().toString('yyyyMMdd') +'.log'), log + '\n', {
            flag:'a'
        })
    }
}

